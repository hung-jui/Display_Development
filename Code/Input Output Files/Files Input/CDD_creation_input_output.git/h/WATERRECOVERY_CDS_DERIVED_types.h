/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_DERIVED_types.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.DERIVED
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_DERIVED_TYPES_H_
#define _WATERRECOVERY_CDS_DERIVED_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	float	CD_POWER; /*  CDS DERIVED CDS POWER  */
	uint32	CD_POWER_VALIDITY; /*  CDS DERIVED CDS POWER VALIDITY  */
	float	THP_POWER; /*  CDS DERIVED THP POWER  */
	uint32	THP_POWER_VALIDITY; /*  CDS DERIVED THP POWER VALIDITY  */
	float	PRODUCTION_RATE; /*  CDS DERIVED PRODUCTION RATE  */
	uint32	PRODUCTION_RATE_VALIDITY; /*  CDS DERIVED PRODUCTION RATE VALIDITY  */
	float	CONSUMPTION_RATE; /*  CDS DERIVED CONSUMPTION RATE  */
	uint32	CONSUMPTION_RATE_VALIDITY; /*  CDS DERIVED CONSUMPTION RATE VALIDITY  */
	float	SPECIFIC_POWER; /*  CDS DERIVED SPECIFIC POWER  */
	uint32	SPECIFIC_POWER_VALIDITY; /*  CDS DERIVED SPECIFIC POWER VALIDITY  */
	float	FEED_TANK_ACCUM; /*  CDS DERIVED FEED  */
	uint32	FEED_TANK_ACCUM_VALIDITY; /*  CDS DERIVED FEED VALIDITY  */
	float	BRINE_TANK_ACCUM; /*  CDS DERIVED BRINE  */
	uint32	BRINE_TANK_ACCUM_VALIDITY; /*  CDS DERIVED BRINE VALIDITY  */
	float	PRODUCT_TANK_ACCUM; /*  CDS DERIVED PRODUCT  */
	uint32	PRODUCT_TANK_ACCUM_VALIDITY; /*  CDS DERIVED PRODUCT VALIDITY  */
} WATERRECOVERY_CDS_DERIVED_TLM_DATA_OUT_t;

#endif