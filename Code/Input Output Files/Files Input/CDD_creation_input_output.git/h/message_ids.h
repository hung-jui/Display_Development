/***********************************************************************************
**	File Name:	message_ids.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to list all command and telemetry message ids that are used by this collaboration
**	The Domains that are participating in this collaboration are:
**	Domain [BATTERY] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/Battery.xml]
**	Domain [PPA] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/PPA.xml]
**	Domain [AMPS] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/AMPS_DualString.xml]
**	Domain [HIVE] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/TTEaudio.xml]
**	Domain [WATERRECOVERY] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/CDS_Gen1.xml]
**	Domain [ASO] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/ASO.xml]
***********************************************************************************/

#ifndef _MESSAGE_IDS_H_
#define _MESSAGE_IDS_H_



#define PPA_ECLSS_AIR_PPA_TLM_MID	0x083B
#define AMPS_MBSU_MBSU_1_TLM_DATA_OUT_MID	0x0802
#define AMPS_MBSU_MBSU_1_CMD_MID	0x1800
#define AMPS_MBSU_MBSU_2_TLM_DATA_OUT_MID	0x0807
#define AMPS_MBSU_MBSU_2_CMD_MID	0x1802
#define AMPS_PDU_PDU_1_TLM_DATA_OUT_MID	0x0809
#define AMPS_PDU_PDU_1_CMD_MID	0x1807
#define AMPS_PDU_PDU_2_TLM_DATA_OUT_MID	0x0811
#define AMPS_PDU_PDU_2_CMD_MID	0x180A
#define HIVE_F_F_AUDIO_AUDIO_1_TLM_DATA_OUT_MID	0x083C
#define HIVE_F_F_AUDIO_AUDIO_1_CMD_MID	0x1819
#define HIVE_HAB_AUDIO_AUDIO_1_TLM_DATA_OUT_MID	0x083D
#define HIVE_HAB_AUDIO_AUDIO_1_CMD_MID	0x181A
#define WATERRECOVERY_CDS_CDS_TLM_DATA_OUT_MID	0x081D
#define WATERRECOVERY_CDS_CDS_CMD_MID	0x1814
#define WATERRECOVERY_CDS_CONDUCTIVITY_TLM_DATA_OUT_MID	0x081E
#define WATERRECOVERY_CDS_DERIVED_TLM_DATA_OUT_MID	0x081F
#define WATERRECOVERY_CDS_FLOWSWITCH_TLM_DATA_OUT_MID	0x0822
#define WATERRECOVERY_CDS_HEATPUMP_TLM_DATA_OUT_MID	0x0827
#define WATERRECOVERY_CDS_PRESSURESENSOR_TLM_DATA_OUT_MID	0x0829
#define WATERRECOVERY_CDS_SOLENOIDVALVE_TLM_DATA_OUT_MID	0x0831
#define WATERRECOVERY_CDS_SOLENOIDVALVE_CMD_MID	0x1815
#define WATERRECOVERY_CDS_THERMOCOUPLE_TLM_DATA_OUT_MID	0x0832
#define WATERRECOVERY_CDS_WEIGHTSCALE_TLM_DATA_OUT_MID	0x0833
#define WATERRECOVERY_FAC_FLOWMETER_TLM_DATA_OUT_MID	0x0834
#define WATERRECOVERY_FAC_FLOWSWITCH_TLM_DATA_OUT_MID	0x0835
#define WATERRECOVERY_FAC_PRESSURETRANSDUCER_TLM_DATA_OUT_MID	0x0836
#define WATERRECOVERY_FAC_PUMP_TLM_DATA_OUT_MID	0x0837
#define WATERRECOVERY_FAC_PUMP_CMD_MID	0x1816
#define WATERRECOVERY_FAC_RELAY_TLM_DATA_OUT_MID	0x0838
#define WATERRECOVERY_FAC_RELAY_CMD_MID	0x1817
#define WATERRECOVERY_FAC_SOLENOIDVALVE_TLM_DATA_OUT_MID	0x0839
#define WATERRECOVERY_FAC_SOLENOIDVALVE_CMD_MID	0x1818
#define WATERRECOVERY_FAC_THERMOCOUPLE_TLM_DATA_OUT_MID	0x083A
#define ASO_PPA_PPAHYDE_PPA_HyDE_Fault_Diag_MID	0x0812
#define ASO_BATTERY_AD_IMS_IMS_STD_ORION_BATT_TLM_MID	0x0813
#define ASO_BATTERY_AD_IMS_IMS_HK_TLM_MID	0x0814
#define ASO_BATTERY_AD_IMS_IMS_OUT_DATA_MID	0x0815
#define ASO_BATTERY_AD_IMS_CMD_MID	0x180E
#define ASO_ACAWS_ACAWS_DE_OUT_IMPACT_REQ_MID	0x0816
#define ASO_ACAWS_ACAWS_DE_HK_TLM__MID	0x0817
#define ASO_ACAWS_ACAWS_DE_OUT_DIAG_MID	0x0818
#define ASO_ACAWS_ACAWS_DE_CMD_MID	0x180F
#define ASO_ACAWS_ACAWS_FD_TEST_RESULTS_MID	0x0819
#define ASO_ACAWS_ACAWS_FD_HK_TLM__MID	0x081A
#define ASO_ACAWS_ACAWS_FD_CMD_MID	0x1812
#define ASO_ACAWS_ACAWS_SE_HK_TLM__MID	0x081B
#define ASO_ACAWS_ACAWS_SE_OUT_DATA_MID	0x081C
#define ASO_ACAWS_ACAWS_SE_CMD_MID	0x1813


#endif