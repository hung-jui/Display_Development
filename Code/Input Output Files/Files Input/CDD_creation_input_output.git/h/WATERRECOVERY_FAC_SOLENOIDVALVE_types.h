/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_SOLENOIDVALVE_types.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.SOLENOIDVALVE
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_SOLENOIDVALVE_TYPES_H_
#define _WATERRECOVERY_FAC_SOLENOIDVALVE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	uint32	SV_CDS_25; /*  FAC SOLENOIDVALVE FEED DRAIN  */
	uint32	SV_CDS_25_VALIDITY; /*  FAC SOLENOIDVALVE FEED DRAIN VALIDITY  */
	uint32	SV_CDS_21; /*  FAC SOLENOIDVALVE CHILLER IN  */
	uint32	SV_CDS_21_VALIDITY; /*  FAC SOLENOIDVALVE CHILLER IN VALIDITY  */
	uint32	SV_CDS_26; /*  FAC SOLENOIDVALVE BRINE DRAIN  */
	uint32	SV_CDS_26_VALIDITY; /*  FAC SOLENOIDVALVE BRINE DRAIN VALIDITY  */
	uint32	SV_CDS_23; /*  FAC SOLENOIDVALVE VACUUM TO CDS  */
	uint32	SV_CDS_23_VALIDITY; /*  FAC SOLENOIDVALVE VACUUM TO CDS VALIDITY  */
	uint32	SV_CDS_22; /*  FAC SOLENOIDVALVE CHILLER OUT  */
	uint32	SV_CDS_22_VALIDITY; /*  FAC SOLENOIDVALVE CHILLER OUT VALIDITY  */
	uint32	SV_CDS_27; /*  FAC SOLENOIDVALVE PRODUCT DRAIN  */
	uint32	SV_CDS_27_VALIDITY; /*  FAC SOLENOIDVALVE PRODUCT DRAIN VALIDITY  */
	uint32	SV_CDS_24; /*  FAC SOLENOIDVALVE FEED FILL  */
	uint32	SV_CDS_24_VALIDITY; /*  FAC SOLENOIDVALVE FEED FILL VALIDITY  */
} WATERRECOVERY_FAC_SOLENOIDVALVE_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} WATERRECOVERY_FAC_SOLENOIDVALVE_NoArgCmd_t;

/*
**	User defined command SV-CDS-25
**	FAC SOLENOIDVALVE FEED DRAIN
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_25_t;

/*
**	User defined command SV-CDS-21
**	FAC SOLENOIDVALVE CHILLER IN
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_21_t;

/*
**	User defined command SV-CDS-26
**	FAC SOLENOIDVALVE BRINE DRAIN
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_26_t;

/*
**	User defined command SV-CDS-23
**	FAC SOLENOIDVALVE VACUUM TO CDS
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_23_t;

/*
**	User defined command SV-CDS-22
**	FAC SOLENOIDVALVE CHILLER OUT
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_22_t;

/*
**	User defined command SV-CDS-27
**	FAC SOLENOIDVALVE PRODUCT DRAIN
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_27_t;

/*
**	User defined command SV-CDS-24
**	FAC SOLENOIDVALVE FEED FILL
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	VALVE_ARG;
} WATERRECOVERY_FAC_SOLENOIDVALVE_SV_CDS_24_t;

#endif