/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_PRESSURETRANSDUCER_types.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.PRESSURETRANSDUCER
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_PRESSURETRANSDUCER_TYPES_H_
#define _WATERRECOVERY_FAC_PRESSURETRANSDUCER_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	float	PT_CDS_07; /*  FAC PRESSURETRANSDUCER VACUUM  */
	uint32	PT_CDS_07_VALIDITY; /*  FAC PRESSURETRANSDUCER VACUUM VALIDITY  */
} WATERRECOVERY_FAC_PRESSURETRANSDUCER_TLM_DATA_OUT_t;

#endif