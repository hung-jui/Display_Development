/***********************************************************************************
**	File Name:	PPA_ECLSS_AIR_PPAREPLAY_types.h
**	Generated on: Fri Jul 10 16:10:10 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface PPA.ECLSS.AIR.PPAREPLAY
***********************************************************************************/

#ifndef _PPA_ECLSS_AIR_PPAREPLAY_TYPES_H_
#define _PPA_ECLSS_AIR_PPAREPLAY_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message PPA-Replay
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	�GCA08_Hydrogen;
	float	�GCA09_Oxygen;
	float	�GCA10_Nitrogen;
	float	�GCA11_Carbon_Monoxide;
	float	�GCA12_Methane;
	float	�GCC13_CO2;
	float	�GCC14_Methane;
	float	�GCC15_Ethylene;
	float	�GCC16_Acetylene;
	uint8	3gPPASVC132;
	uint8	3gPPASVC133;
	uint8	3gPPASVC134;
	uint8	SABSV_220;
	uint8	SABSV_221;
	float	3gPPA_CH4/CO2_cmd;
	float	3gPPA_H2_main_cmd;
	float	3gPPA_H2_viewport_cmd;
	float	3gPPA_FC104;
	float	3gPPA_FC105;
	float	3gPPA_FC106;
	float	3gPPA_FT104;
	float	3gPPA_FT105;
	float	3gPPA_FT106;
	float	3gPPA_reactor_P_cmd;
	float	3gPPA_PT110;
	float	3gPPA_P115;
	float	3gPPA_P116;
	uint8	3gPPA_CO2_regen_cmd;
	uint8	3gPPAuWstart;
	float	3gPPA_CO2_plasma_uW_cmd;
	float	3gPPA_MeP_uW_cmd;
	float	3gPPA_Input_Power;
	float	3gPPA_Refl_Power;
	uint32	3gPPAstub1posn;
	uint32	3gPPAstub2posn;
	uint32	3gPPAstub3posn;
	uint8	3gPPAuWabort;
	float	3gPPA_T120;
	float	3gPPA_T121;
	float	3gPPA_T122;
	float	3gPPA_T123;
	float	3gPPA_T124;
	float	3gPPA_T125;
	float	3gPPA_T126;
	uint8	3gPPA_LEL_Alarm_State;
	uint8	3gPPA_Over_Pressure/Temp_State;
	uint8	CRA_SVC007;
	uint8	CRA_SVC102;
	uint8	CRA_SVO203;
	float	CRA_P002;
	float	CRA_P106;
	float	CRA_P206;
	float	CRA_MFC005_command;
	float	CRA_MFC005_flow;
	float	CRA_MFC103_command;
	float	CRA_MFC103_flow;
	float	CRA_Molar_Ratio;
	float	CRA_P006-1;
	float	CRA_P006-2;
	uint8	CRA_H402-1;
	uint8	CRA_H402-2;
	uint8	CRA_FAN503;
	float	CRA_T403-1;
	float	CRA_T403-2;
	float	CRA_T404-1;
	float	CRA_T404-2;
	float	CRA_P601;
	float	CRA_DP405;
	uint8	CRA_FAN502;
	float	CRA_T407-1;
	float	CRA_T407-2;
	float	CRA_DP409;
	uint8	CRA_SVC302/303;
	uint8	CRA_PUMP301;
	uint8	CRA_SVO608;
	uint8	CRA_SVO604;
	uint8	CRA_SVO610;
	float	CRA_CG411;
	float	CRA_CG412;
} PPA_ECLSS_AIR_PPAREPLAY_PPA-Replay_t;

#endif