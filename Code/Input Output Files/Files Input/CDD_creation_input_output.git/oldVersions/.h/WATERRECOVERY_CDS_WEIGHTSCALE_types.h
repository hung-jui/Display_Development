/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_WEIGHTSCALE_types.h
**	Generated on: Fri Jul 10 16:10:10 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.WEIGHTSCALE
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_WEIGHTSCALE_TYPES_H_
#define _WATERRECOVERY_CDS_WEIGHTSCALE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	M1; // CDS WEIGHTSCALE FEED TANK
	uint32	M1_VALIDITY; // CDS WEIGHTSCALE FEED TANK VALIDITY
	float	M2; // CDS WEIGHTSCALE BRINE TANK
	uint32	M2_VALIDITY; // CDS WEIGHTSCALE BRINE TANK VALIDITY
	float	M3; // CDS WEIGHTSCALE PRODUCT TANK
	uint32	M3_VALIDITY; // CDS WEIGHTSCALE PRODUCT TANK VALIDITY
} WATERRECOVERY_CDS_WEIGHTSCALE_TLM_DATA_OUT_t;

#endif