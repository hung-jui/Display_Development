/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_FLOWMETER_types.h
**	Generated on: Fri Jul 10 16:10:10 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.FLOWMETER
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_FLOWMETER_TYPES_H_
#define _WATERRECOVERY_FAC_FLOWMETER_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	FM-CDS-02; // FAC FLOWMETER COOLANT LOOP
	uint32	FM-CDS-02_VALIDITY; // FAC FLOWMETER COOLANT LOOP VALIDITY
} WATERRECOVERY_FAC_FLOWMETER_TLM_DATA_OUT_t;

#endif