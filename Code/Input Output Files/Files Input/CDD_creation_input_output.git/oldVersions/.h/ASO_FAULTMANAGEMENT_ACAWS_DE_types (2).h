/***********************************************************************************
**	File Name:	ASO_FAULTMANAGEMENT_ACAWS_DE_types.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ASO.FAULTMANAGEMENT.ACAWS_DE
***********************************************************************************/

#ifndef _ASO_FAULTMANAGEMENT_ACAWS_DE_TYPES_H_
#define _ASO_FAULTMANAGEMENT_ACAWS_DE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message OUT_IMPACT_REQ
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	mode_aware;
	uint16	unique_instance_id;
	uint16	failure_list_cnt;
	char	failure_list[240];
} ASO_FAULTMANAGEMENT_ACAWS_DE_OUT_IMPACT_REQ_t;

/*
**	Struct definition for message OUT_DIAG
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	source_id;
	uint16	sesssion_id;
	uint16	unique_instance_id;
	uint16	aspect_count;
	char	System_HealthStatus[40];
} ASO_FAULTMANAGEMENT_ACAWS_DE_OUT_DIAG_t;

#endif