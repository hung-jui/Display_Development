/***********************************************************************************
**	File Name:	BATTERY_ASO_ORB_DATA_GEN_types.h
**	Generated on: Wed Jul 22 12:58:48 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface BATTERY.ASO.ORB_DATA_GEN
***********************************************************************************/

#ifndef _BATTERY_ASO_ORB_DATA_GEN_TYPES_H_
#define _BATTERY_ASO_ORB_DATA_GEN_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message ORB_DATA_GEN_HK_TLM
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	ORB_DATA_GEN_HK_TLM; // In fact Contribution is an array of 48 doubles one for each battery output, can't be produced by CDD process
} BATTERY_ASO_ORB_DATA_GEN_ORB_DATA_GEN_HK_TLM_t;

/*
**	Struct definition for message ORB_DATA_GEN_OUT_ORION_BATT_DATA
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	ORB_DATA_GEN_OUT_ORION_BATT_DATA;
} BATTERY_ASO_ORB_DATA_GEN_ORB_DATA_GEN_OUT_ORION_BATT_DATA_t;

/*
**	Struct definition for message ORB_DATA_GEN_OUT_DATA
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	Battery_C1a_Return_Leg_Current_Sample1;
	float	Battery_C1a_Cell_01_Voltage1;
	float	Battery_C1a_Cell_02_Voltage1;
	float	Battery_C1a_Cell_03_Voltage1;
	float	Battery_C1a_Cell_04_Voltage1;
	float	Battery_C1a_Cell_05_Voltage1;
	float	Battery_C1a_Cell_06_Voltage1;
	float	Battery_C1a_Cell_07_Voltage1;
	float	Battery_C1a_Cell_08_Voltage1;
	float	Battery_C1a_Cell_09_Voltage1;
	float	Battery_C1a_Cell_10_Voltage1;
	float	Battery_C1a_Cell_11_Voltage1;
	float	Battery_C1a_Cell_12_Voltage1;
	float	Battery_C1a_Cell_13_Voltage1;
	float	Battery_C1a_Cell_14_Voltage1;
	float	Battery_C1a_Cell_15_Voltage1;
	float	Battery_C1a_Cell_16_Voltage1;
	float	Battery_C1a_Cell_17_Voltage1;
	float	Battery_C1a_Cell_18_Voltage1;
	float	Battery_C1a_Cell_19_Voltage1;
	float	Battery_C1a_Cell_20_Voltage1;
	float	Battery_C1a_Cell_21_Voltage1;
	float	Battery_C1a_Cell_22_Voltage1;
	float	Battery_C1a_Cell_23_Voltage1;
	float	Battery_C1a_Cell_24_Voltage1;
	float	Battery_C1a_Cell_25_Voltage1;
	float	Battery_C1a_Cell_26_Voltage1;
	float	Battery_C1a_Cell_27_Voltage1;
	float	Battery_C1a_Cell_28_Voltage1;
	float	Battery_C1a_Cell_29_Voltage1;
	float	Battery_C1a_Cell_30_Voltage1;
	float	Battery_C1a_Cell_31_Voltage1;
	float	Battery_C1a_Cell_32_Voltage1;
	float	Battery_C1a_Temp_1_1;
	float	Battery_C1a_Temp_2_1;
	float	Battery_C1a_Temp_3_1;
	float	Battery_C1a_Temp_4_1;
	float	Battery_C1a_Total_Voltage_Sample1;
} BATTERY_ASO_ORB_DATA_GEN_ORB_DATA_GEN_OUT_DATA_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} BATTERY_ASO_ORB_DATA_GEN_NoArgCmd_t;

/*
**	User defined command ORB_DATA_GEN_CMD
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} BATTERY_ASO_ORB_DATA_GEN_ORB_DATA_GEN_CMD_t;

/*
**	User defined command ORB_DATA_GEN_HK
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} BATTERY_ASO_ORB_DATA_GEN_ORB_DATA_GEN_HK_t;

#endif