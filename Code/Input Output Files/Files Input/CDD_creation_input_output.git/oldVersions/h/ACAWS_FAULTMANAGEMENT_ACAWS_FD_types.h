/***********************************************************************************
**	File Name:	ACAWS_FAULTMANAGEMENT_ACAWS_FD_types.h
**	Generated on: Wed Jul 22 12:58:49 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ACAWS.FAULTMANAGEMENT.ACAWS_FD
***********************************************************************************/

#ifndef _ACAWS_FAULTMANAGEMENT_ACAWS_FD_TYPES_H_
#define _ACAWS_FAULTMANAGEMENT_ACAWS_FD_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TEST_RESULTS
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	char	System_TestResult[40];
} ACAWS_FAULTMANAGEMENT_ACAWS_FD_TEST_RESULTS_t;

/*
**	Struct definition for message HK_TLM 
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	char	acaws_fd_hk[64]; // need clarification
} ACAWS_FAULTMANAGEMENT_ACAWS_FD_HK_TLM _t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_FD_NoArgCmd_t;

/*
**	User defined command acaws_fd_cmd
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_FD_acaws_fd_cmd_t;

/*
**	User defined command acaws_fd_wakeup
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_FD_acaws_fd_wakeup_t;

/*
**	User defined command acaws_fd_snd_hk
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_FD_acaws_fd_snd_hk_t;

#endif