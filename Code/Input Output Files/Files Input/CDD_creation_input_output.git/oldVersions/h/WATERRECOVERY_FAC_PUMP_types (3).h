/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_PUMP_types.h
**	Generated on: Fri Jul 31 13:00:43 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.PUMP
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_PUMP_TYPES_H_
#define _WATERRECOVERY_FAC_PUMP_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	uint32	P_CDS_16; /*  FAC PUMP BRINE  */
	uint32	P_CDS_16_VALIDITY; /*  FAC PUMP BRINE VALIDITY  */
	uint32	P_CDS_05; /*  FAC PUMP VACUUM  COMPRESSOR  */
	uint32	P_CDS_05_VALIDITY; /*  FAC PUMP VACUUM  COMPRESSOR VALIDITY  */
	uint32	P_CDS_14; /*  FAC PUMP FEED IN  */
	uint32	P_CDS_14_VALIDITY; /*  FAC PUMP FEED IN VALIDITY  */
	uint32	P_CDS_15; /*  FAC PUMP FEED OUT  */
	uint32	P_CDS_15_VALIDITY; /*  FAC PUMP FEED OUT VALIDITY  */
	uint32	P_CDS_17; /*  FAC PUMP PRODUCT  */
	uint32	P_CDS_17_VALIDITY; /*  FAC PUMP PRODUCT VALIDITY  */
} WATERRECOVERY_FAC_PUMP_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} WATERRECOVERY_FAC_PUMP_NoArgCmd_t;

/*
**	User defined command P-CDS-16
**	FAC PUMP BRINE
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	PUMP_ARG;
} WATERRECOVERY_FAC_PUMP_P_CDS_16_t;

/*
**	User defined command P-CDS-05
**	FAC PUMP VACUUM 
COMPRESSOR
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	PUMP_ARG;
} WATERRECOVERY_FAC_PUMP_P_CDS_05_t;

/*
**	User defined command P-CDS-14
**	FAC PUMP FEED IN
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	PUMP_ARG;
} WATERRECOVERY_FAC_PUMP_P_CDS_14_t;

/*
**	User defined command P-CDS-15
**	FAC PUMP FEED OUT
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	PUMP_ARG;
} WATERRECOVERY_FAC_PUMP_P_CDS_15_t;

/*
**	User defined command P-CDS-17
**	FAC PUMP PRODUCT
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	PUMP_ARG;
} WATERRECOVERY_FAC_PUMP_P_CDS_17_t;

#endif