/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_CONDUCTIVITY_types.h
**	Generated on: Wed Jul 22 12:58:49 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.CONDUCTIVITY
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_CONDUCTIVITY_TYPES_H_
#define _WATERRECOVERY_CDS_CONDUCTIVITY_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	C1; // CDS CONDUCTIVITY PRODUCT TANK CD
	uint32	C1_VALIDITY; // CDS CONDUCTIVITY PRODUCT TANK CD VALIDITY
	float	C2; // CDS CONDUCTIVITY COLD LOOP CD
	uint32	C2_VALIDITY; // CDS CONDUCTIVITY COLD LOOP CD VALIDITY
} WATERRECOVERY_CDS_CONDUCTIVITY_TLM_DATA_OUT_t;

#endif