/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_HEATPUMP_types.h
**	Generated on: Thu Jul 16 10:21:19 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.HEATPUMP
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_HEATPUMP_TYPES_H_
#define _WATERRECOVERY_CDS_HEATPUMP_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	THP-SHUNT; // CDS HEATPUMP THP SHUNT
	uint32	THP-SHUNT_VALIDITY; // CDS HEATPUMP THP SHUNT VALIDITY
	float	THP-VOLTS; // CDS HEATPUMP THP VOLTS
	uint32	THP-VOLTS_VALIDITY; // CDS HEATPUMP THP VOLTS VALIDITY
	float	PSU-CDS-3-VOLTS; // CDS HEATPUMP PSU CDS 3 VOLTS
	uint32	PSU-CDS-3-VOLTS_VALIDITY; // CDS HEATPUMP PSU CDS 3 VOLTS VALIDITY
	float	PSU-CDS-3-CURRENT; // CDS HEATPUMP PSU CDS 3 CURRENT
	uint32	PSU-CDS-3-CURRENT_VALIDITY; // CDS HEATPUMP PSU CDS 3 CURRENT VALIDITY
} WATERRECOVERY_CDS_HEATPUMP_TLM_DATA_OUT_t;

#endif