/***********************************************************************************
**	File Name:	HIVE_F_F_AUDIO_AUDIO_1_types.h
**	Generated on: Fri Jul 31 13:00:43 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface HIVE.F_F.AUDIO.AUDIO_1
***********************************************************************************/

#ifndef _HIVE_F_F_AUDIO_AUDIO_1_TYPES_H_
#define _HIVE_F_F_AUDIO_AUDIO_1_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	char	STATUS_UPDATE[450]; /*  sends an 'ok' if command was successful  */
	char	PACKET_SIZE[450];
	char	CHANNEL_INFO[450];
	char	LOCAL_RTP_PORT[450];
	char	USER_INFO[450]; /*  shows possible users and logged in users  */
	char	USER_EXT[450]; /*  The users extension  */
	char	HEADSET_CONNECTIONS[450]; /*  Shows the possible headset connections  */
	char	SIMULCAST[450];
	char	MULTICAST[450];
	char	VADTX[450];
} HIVE_F_F_AUDIO_AUDIO_1_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_NoArgCmd_t;

/*
**	User defined command CREW_LOGIN_ID
**	Crew member unique login ID
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_CREW_LOGIN_ID_t;

/*
**	User defined command SERVER_PING_REQUEST
**	Sends ping
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_SERVER_PING_REQUEST_t;

/*
**	User defined command SET_VERSION
**	Sets the interface version
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_SET_VERSION_t;

/*
**	User defined command INITIATE_PTT
**	Begin talking on interface
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_INITIATE_PTT_t;

/*
**	User defined command TERMINATE_PTT
**	Stop talking on interface
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_TERMINATE_PTT_t;

/*
**	User defined command SET_VOLUME
**	Set volume of software
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_SET_VOLUME_t;

/*
**	User defined command SET_DEVICE
**	set the output type
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_SET_DEVICE_t;

/*
**	User defined command SET_HEADSET
**	select the headset
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_SET_HEADSET_t;

/*
**	User defined command CLOSE_CONNECTION
**	closes in a stable way
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_CLOSE_CONNECTION_t;

/*
**	User defined command INITIATE_SIP
**	begins a talk to a SIP server
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} HIVE_F_F_AUDIO_AUDIO_1_INITIATE_SIP_t;

#endif