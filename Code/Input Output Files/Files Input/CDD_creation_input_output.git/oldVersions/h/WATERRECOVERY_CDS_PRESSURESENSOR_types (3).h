/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_PRESSURESENSOR_types.h
**	Generated on: Fri Jul 31 13:00:43 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.PRESSURESENSOR
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_PRESSURESENSOR_TYPES_H_
#define _WATERRECOVERY_CDS_PRESSURESENSOR_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	float	P3; /*  CDS PRESSURESENSOR BRINE  */
	uint32	P3_VALIDITY; /*  CDS PRESSURESENSOR BRINE VALIDITY  */
	float	P2; /*  CDS PRESSURESENSOR CD  */
	uint32	P2_VALIDITY; /*  CDS PRESSURESENSOR CD VALIDITY  */
	float	P4; /*  CDS PRESSURESENSOR PRODUCT  */
	uint32	P4_VALIDITY; /*  CDS PRESSURESENSOR PRODUCT VALIDITY  */
} WATERRECOVERY_CDS_PRESSURESENSOR_TLM_DATA_OUT_t;

#endif