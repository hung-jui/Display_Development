/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_THERMOCOUPLE_types.h
**	Generated on: Thu Jul 16 10:21:19 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.THERMOCOUPLE
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_THERMOCOUPLE_TYPES_H_
#define _WATERRECOVERY_FAC_THERMOCOUPLE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	TT-CDS-04; // FAC THERMOCOUPLE COOLANT
	uint32	TT-CDS-04_VALIDITY; // FAC THERMOCOUPLE COOLANT VALIDITY
	float	FAC-7409TB-1-CJ; // FAC THERMOCOUPLE FAC 7409TB 1 CJ
	uint32	FAC-7409TB-1-CJ_VALIDITY; // FAC THERMOCOUPLE FAC 7409TB 1 CJ VALIDITY
} WATERRECOVERY_FAC_THERMOCOUPLE_TLM_DATA_OUT_t;

#endif