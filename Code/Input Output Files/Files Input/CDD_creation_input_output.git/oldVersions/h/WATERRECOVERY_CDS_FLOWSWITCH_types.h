/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_FLOWSWITCH_types.h
**	Generated on: Thu Jul 16 10:21:19 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.FLOWSWITCH
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_FLOWSWITCH_TYPES_H_
#define _WATERRECOVERY_CDS_FLOWSWITCH_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	FH; // CDS FLOWSWITCH HOT LOOP
	uint32	FH_VALIDITY; // CDS FLOWSWITCH HOT LOOP VALIDITY
	uint32	FC; // CDS FLOWSWITCH COLD LOOP
	uint32	FC_VALIDITY; // CDS FLOWSWITCH COLD LOOP VALIDITY
} WATERRECOVERY_CDS_FLOWSWITCH_TLM_DATA_OUT_t;

#endif