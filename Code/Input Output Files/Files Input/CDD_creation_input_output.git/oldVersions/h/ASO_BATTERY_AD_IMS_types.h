/***********************************************************************************
**	File Name:	ASO_BATTERY_AD_IMS_types.h
**	Generated on: Fri Jul 31 13:00:43 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ASO.BATTERY.AD_IMS
***********************************************************************************/

#ifndef _ASO_BATTERY_AD_IMS_TYPES_H_
#define _ASO_BATTERY_AD_IMS_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message IMS_STD_ORION_BATT_TLM
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	uint32	TBD;
} ASO_BATTERY_AD_IMS_IMS_STD_ORION_BATT_TLM_t;

/*
**	Struct definition for message IMS_HK_TLM
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	uint32	TLM;
} ASO_BATTERY_AD_IMS_IMS_HK_TLM_t;

/*
**	Struct definition for message IMS_OUT_DATA
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	float	Timestamp;
	uint8	Context;
	float	Anomaly_score;
	uint8	Anomaly;
	float	Contribution;
} ASO_BATTERY_AD_IMS_IMS_OUT_DATA_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_BATTERY_AD_IMS_NoArgCmd_t;

/*
**	User defined command IMS_CMD
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_BATTERY_AD_IMS_IMS_CMD_t;

/*
**	User defined command IMS_SEND_HK
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_BATTERY_AD_IMS_IMS_SEND_HK_t;

#endif