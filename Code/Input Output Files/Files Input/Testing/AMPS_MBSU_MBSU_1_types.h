/***********************************************************************************
**	File Name:	AMPS_MBSU_MBSU_1_types.h
**	Date: Tue May 12 12:15:25 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface AMPS.MBSU.MBSU_1
***********************************************************************************/

#ifndef _AMPS_MBSU_MBSU_1_TYPES_H_
#define _AMPS_MBSU_MBSU_1_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	MBSU_ID; // unique ID of the component
	uint8	MBSU_INPUT_SOURCESELECT_STATUS; // Shows state of 6 bit pattern input register
	uint8	MBSU_INPUTRELAY_FEEDBACK_STATUS; // Shows the actual relay state of 6 bit pattern, read from feedback contacts.
	uint8	MBSU_INPUT_INVALIDBATTERY_ERROR; // 0= OK , 1= Indicates that the Auxilary MBSU port is NOT in OFF state when selecting Battery)
	uint8	MBSU_INPUT_INVALIDSOLARARRAY_ERROR; // 0=OK, 1= Indicates that the Auxilary MBSU port is NOT in OFF state when selecting Solar Array)
	uint8	MBSU_INPUT_INVALIDAUXMBSUINPUT_ERROR; // 0=OK 1= Indicates that the SolarArray OR Battery NOT in OFF state when selecting AuxMBSU as Source)
	uint8	MBSU_INPUT_INVALIDAUXMBSUOUTPUT_ERROR; // 0=OK, 1= Indicates that AuxMBSU has a voltage when selecting AuxMBSU as Output)
	uint8	MBSU_INPUT_RELAYSTATEMISMATCH_ERROR; // 0=OK, 1= ERROR (Indicates that the input status and input relay feedback states are mismatched (relay or driver error))
	uint8	MBSU_INPUT_POSNEGSTATEMISMATCH_ERROR; // 0=OK, 1= Indicates that Pos and Neg relay contact states are mismatched (relay or driver error)
	uint8	MBSU_BUSOUTSTATUS; // Shows state of 8 bit output register that drives relays
	uint8	MBSU_BUSOUTRELAYFEEDBACKSTATUS; // Shows the actual 8 bit relay state of read from feedback contacts.)
	uint8	MBSU_BUSOUTRELAYSTATEMISMATCHERROR; // 0=OK, 1= ERROR ( Indicates that the input status and input relay feedback states are mismatched (relay or driver error))
	uint8	MBSU_BUSOUTPOSNEGSTATEMISMATCHERROR; // 0=OK, 1= ERROR (Indicates that Pos and Neg relay contact states are mismatched (relay or driver error)
	uint8	MBSU_INPUTRELAYTRIPSTATE; // relay trip state of input relays
	uint8	MBSU_OUTPUTRELAYTRIPSTATE; // relay trip state of output relays
	double	MBSU_SOLAR_ARRAY_INPUT_CURRENT; // Current from Solar Array
	double	MBSU_BATTERY_BIDIRECTIONAL_CURRENT; // Current to/from Battery (bi directional charge ?discharge current)
	double	MBSU_AUXMBSU_BIDIRECTIONAL_CURRENT; // Current to/from the AuxMBSU Port
	double	MBSU_AUXMBSU_OUTPUT_VOLTAGE; // Voltage of the AuxMBSU Port (NOTE: independent of current polarity)
	double	MBSU_HKPS_VOLTAGE; // Voltage of the house keeping power supply (1 measurement covers 3 supplies)
	double	MBSU_OUTPUT_CURRENT_1; // MBSUOutputCurrent1
	double	MBSU_OUTPUT_CURRENT_2; // MBSUOutputCurrent2
	double	MBSU_OUTPUT_CURRENT_3; // MBSUOutputCurrent3
	double	MBSU_OUTPUT_CURRENT_4; // MBSUOutputCurrent4
	double	MBSU_SARVOLTAGE; // solar array regulator voltage
	double	MBSU_BATTVOLTAGE; // battery input voltage
	double	MBSU_INNERVOLTAGE; // inner bus voltage
	double	MBSU_SARCURRTRIPSP; // Trip setpoint for SAR input
	double	MBSU_BATTCURRTRIPSP; // Trip setpoint for battery input
	double	MBSU_AUXCURRTRIPSP; // Trip setpoint for Aux input
	double	MBSU_OUTPUT1CURRTRIPSP; // output ch1 trip setpoint
	double	MBSU_OUTPUT2CURRTRIPSP; // output ch2 trip setpoint
	double	MBSU_OUTPUT3CURRTRIPSP; // output ch3 trip setpoint
	double	MBSU_OUTPUT4CURRTRIPSP; // output ch4 trip setpoint
} AMPS_MBSU_MBSU_1_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} AMPS_MBSU_MBSU_1_NoArgCmd_t;

/*
**	User defined command SET_INPUT_SOURCE_SELECT
**	select input bus to MBSU
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	SOLARARRAY_POS;
	uint8	BATTERY_POS;
	uint8	AUXILARY_POS;
} AMPS_MBSU_MBSU_1_SET_INPUT_SOURCE_SELECT_t;

/*
**	User defined command SET_MBSU_OUTPUT_CHANNEL_STATE
**	select output channel of MBSU, which PDU
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	OUTPUTCHANNEL_1_POS;
	uint8	OUTPUTCHANNEL_2_POS;
	uint8	OUTPUTCHANNEL_3_POS;
} AMPS_MBSU_MBSU_1_SET_MBSU_OUTPUT_CHANNEL_STATE_t;

/*
**	User defined command SET_MBSU_Input_Curr_Limit 
**	artificial input current limit
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	double	Input_Curr_Limit_SP;
	uint8	UpdateEnableMask;
} AMPS_MBSU_MBSU_1_SET_MBSU_Input_Curr_Limit _t;

/*
**	User defined command SET_MBSU_Output_Curr_Limit
**	artificial output current limit
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	double	Output_Curr_Limit_SP;
	uint8	UpdateEnableMask;
} AMPS_MBSU_MBSU_1_SET_MBSU_Output_Curr_Limit_t;

/*
**	User defined command CLEAR_MBSU_Input_Curr_Trip
**	reset modified current limit to default
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	ClearTripMask;
} AMPS_MBSU_MBSU_1_CLEAR_MBSU_Input_Curr_Trip_t;

/*
**	User defined command CLEAR_MBSU_Output_Curr_Trip
**	reset modified current limit to default
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	ClearTripMask;
} AMPS_MBSU_MBSU_1_CLEAR_MBSU_Output_Curr_Trip_t;

#endif