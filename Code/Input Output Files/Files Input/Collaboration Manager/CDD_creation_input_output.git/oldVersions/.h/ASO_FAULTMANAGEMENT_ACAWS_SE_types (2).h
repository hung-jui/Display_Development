/***********************************************************************************
**	File Name:	ASO_FAULTMANAGEMENT_ACAWS_SE_types.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ASO.FAULTMANAGEMENT.ACAWS_SE
***********************************************************************************/

#ifndef _ASO_FAULTMANAGEMENT_ACAWS_SE_TYPES_H_
#define _ASO_FAULTMANAGEMENT_ACAWS_SE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message OUT_IMPACT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint16	unique_instance_id;
	char	acaws_se_effects[960]; // acaws_se_effects is of type ACAWS_SE_Effects_t, which in turn is an array of type ACAWS_SE_ImpactType_t (size = 4 bytes). The max size of the array is 24 making this field 24*4 = 96 bytes. Then we multiple by DIAG_MAX_ELEM_SIZE that currently is 10.
} ASO_FAULTMANAGEMENT_ACAWS_SE_OUT_IMPACT_t;

#endif