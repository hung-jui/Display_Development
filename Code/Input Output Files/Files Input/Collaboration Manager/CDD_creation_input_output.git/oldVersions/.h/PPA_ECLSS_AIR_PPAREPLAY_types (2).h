/***********************************************************************************
**	File Name:	PPA_ECLSS_AIR_PPAREPLAY_types.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface PPA.ECLSS.AIR.PPAREPLAY
***********************************************************************************/

#ifndef _PPA_ECLSS_AIR_PPAREPLAY_TYPES_H_
#define _PPA_ECLSS_AIR_PPAREPLAY_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message PPA_Replay
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	microGCA08_Hydrogen;
	float	microGCA09_Oxygen;
	float	microGCA10_Nitrogen;
	float	microGCA11_Carbon_Monoxide;
	float	microGCA12_Methane;
	float	microGCC13_CO2;
	float	microGCC14_Methane;
	float	microGCC15_Ethylene;
	float	microGCC16_Acetylene;
	uint8	three_gPPASVC132;
	uint8	three_gPPASVC133;
	uint8	three_gPPASVC134;
	uint8	SABSV_220;
	uint8	SABSV_221;
	float	three_gPPA_CH4_CO2_cmd;
	float	three_gPPA_H2_main_cmd;
	float	three_gPPA_H2_viewport_cmd;
	float	three_gPPA_FC104;
	float	three_gPPA_FC105;
	float	three_gPPA_FC106;
	float	three_gPPA_FT104;
	float	three_gPPA_FT105;
	float	three_gPPA_FT106;
	float	three_gPPA_reactor_P_cmd;
	float	three_gPPA_PT110;
	float	three_gPPA_P115;
	float	three_gPPA_P116;
	uint8	three_gPPA_CO2_regen_cmd;
	uint8	three_gPPAuWstart;
	float	three_gPPA_CO2_plasma_uW_cmd;
	float	three_gPPA_MeP_uW_cmd;
	float	three_gPPA_Input_Power;
	float	three_gPPA_Refl_Power;
	uint32	three_gPPAstub1posn;
	uint32	three_gPPAstub2posn;
	uint32	three_gPPAstub3posn;
	uint8	three_gPPAuWabort;
	float	three_gPPA_T120;
	float	three_gPPA_T121;
	float	three_gPPA_T122;
	float	three_gPPA_T123;
	float	three_gPPA_T124;
	float	three_gPPA_T125;
	float	three_gPPA_T126;
	uint8	three_gPPA_LEL_Alarm_State;
	uint8	three_gPPA_Over_Pressure_Temp_State;
	uint8	CRA_SVC007;
	uint8	CRA_SVC102;
	uint8	CRA_SVO203;
	float	CRA_P002;
	float	CRA_P106;
	float	CRA_P206;
	float	CRA_MFC005_command;
	float	CRA_MFC005_flow;
	float	CRA_MFC103_command;
	float	CRA_MFC103_flow;
	float	CRA_Molar_Ratio;
	float	CRA_P006_1;
	float	CRA_P006_2;
	uint8	CRA_H402_1;
	uint8	CRA_H402_2;
	uint8	CRA_FAN503;
	float	CRA_T403_1;
	float	CRA_T403_2;
	float	CRA_T404_1;
	float	CRA_T404_2;
	float	CRA_P601;
	float	CRA_DP405;
	uint8	CRA_FAN502;
	float	CRA_T407_1;
	float	CRA_T407_2;
	float	CRA_DP409;
	uint8	CRA_SVC302_303;
	uint8	CRA_PUMP301;
	uint8	CRA_SVO608;
	uint8	CRA_SVO604;
	uint8	CRA_SVO610;
	float	CRA_CG411;
	float	CRA_CG412;
} PPA_ECLSS_AIR_PPAREPLAY_PPA_Replay_t;

#endif