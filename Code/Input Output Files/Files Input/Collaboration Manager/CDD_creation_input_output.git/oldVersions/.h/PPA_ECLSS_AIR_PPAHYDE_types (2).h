/***********************************************************************************
**	File Name:	PPA_ECLSS_AIR_PPAHYDE_types.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface PPA.ECLSS.AIR.PPAHYDE
***********************************************************************************/

#ifndef _PPA_ECLSS_AIR_PPAHYDE_TYPES_H_
#define _PPA_ECLSS_AIR_PPAHYDE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message PPA_HyDE_Fault_Diag
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	has_fault;
	float	time_fault;
	char	compoent[64];
	char	mode[64];
} PPA_ECLSS_AIR_PPAHYDE_PPA_HyDE_Fault_Diag_t;

#endif