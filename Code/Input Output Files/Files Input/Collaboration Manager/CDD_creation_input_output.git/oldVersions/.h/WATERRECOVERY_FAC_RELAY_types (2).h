/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_RELAY_types.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.RELAY
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_RELAY_TYPES_H_
#define _WATERRECOVERY_FAC_RELAY_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	CH-CDS-01; // FAC RELAY CHILLER
	uint32	CH-CDS-01_VALIDITY; // FAC RELAY CHILLER VALIDITY
	uint32	FD-CDS-09; // FAC RELAY VAPOR TRAP
	uint32	FD-CDS-09_VALIDITY; // FAC RELAY VAPOR TRAP VALIDITY
} WATERRECOVERY_FAC_RELAY_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} WATERRECOVERY_FAC_RELAY_NoArgCmd_t;

/*
**	User defined command CH-CDS-01
**	FAC RELAY CHILLER
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
} WATERRECOVERY_FAC_RELAY_CH-CDS-01_t;

/*
**	User defined command FD-CDS-09
**	FAC RELAY VAPOR TRAP
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
	uint8	CONTROL_ARG;
} WATERRECOVERY_FAC_RELAY_FD-CDS-09_t;

#endif