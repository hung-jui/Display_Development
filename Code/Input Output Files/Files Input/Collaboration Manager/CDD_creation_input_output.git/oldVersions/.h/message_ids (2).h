/***********************************************************************************
**	File Name:	message_ids.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to list all command and telemetry message id that are used by this collaboration
**	The Domains that are participating in this collaboration are:
**	Domain [PPA] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/ASOforPPA.xml]
**	Domain [AMPS] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/AMPS_DualString.xml]
**	Domain [HIVE] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/TTE_Audio.xml]
**	Domain [WATERRECOVERY] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/CDS_Gen1.xml]
**	Domain [ASO] from resource [file:/C:/Repositories/SysML_projects/CDD_creation_input_output/CDD_Import_Completed_spreadsheets/ACAWSforAMPS.xml]
***********************************************************************************/

#ifndef _MESSAGE_IDS_H_
#define _MESSAGE_IDS_H_



#define PPA_ECLSS_AIR_PPAHYDE_PPA_HyDE_Fault_Diag_MID	0x0816
#define PPA_ECLSS_AIR_PPAREPLAY_PPA_Replay_MID	0x0817
#define AMPS_MBSU_MBSU_1_TLM_DATA_OUT_MID	0x0812
#define AMPS_MBSU_MBSU_1_CMD_MID	0x1800
#define AMPS_MBSU_MBSU_2_TLM_DATA_OUT_MID	0x0813
#define AMPS_PDU_PDU_1_TLM_DATA_OUT_MID	0x0814
#define AMPS_PDU_PDU_1_CMD_MID	0x1802
#define AMPS_PDU_PDU_2_TLM_DATA_OUT_MID	0x0815
#define HIVE_F_F_AUDIO_AUDIO_1_TLM_DATA_OUT_MID	0x0836
#define HIVE_F_F_AUDIO_AUDIO_1_CMD_MID	0x1813
#define HIVE_HAB_AUDIO_AUDIO_1_TLM_DATA_OUT_MID	0x0837
#define HIVE_HAB_AUDIO_AUDIO_1_CMD_MID	0x1814
#define WATERRECOVERY_CDS_CDS_TLM_DATA_OUT_MID	0x0818
#define WATERRECOVERY_CDS_CDS_CMD_MID	0x1807
#define WATERRECOVERY_CDS_CONDUCTIVITY_TLM_DATA_OUT_MID	0x0819
#define WATERRECOVERY_CDS_DERIVED_TLM_DATA_OUT_MID	0x081A
#define WATERRECOVERY_CDS_FLOWSWITCH_TLM_DATA_OUT_MID	0x081B
#define WATERRECOVERY_CDS_HEATPUMP_TLM_DATA_OUT_MID	0x081C
#define WATERRECOVERY_CDS_PRESSURESENSOR_TLM_DATA_OUT_MID	0x081D
#define WATERRECOVERY_CDS_SOLENOIDVALVE_TLM_DATA_OUT_MID	0x081E
#define WATERRECOVERY_CDS_SOLENOIDVALVE_CMD_MID	0x180A
#define WATERRECOVERY_CDS_THERMOCOUPLE_TLM_DATA_OUT_MID	0x081F
#define WATERRECOVERY_CDS_WEIGHTSCALE_TLM_DATA_OUT_MID	0x0822
#define WATERRECOVERY_FAC_FLOWMETER_TLM_DATA_OUT_MID	0x0827
#define WATERRECOVERY_FAC_FLOWSWITCH_TLM_DATA_OUT_MID	0x0829
#define WATERRECOVERY_FAC_PRESSURETRANSDUCER_TLM_DATA_OUT_MID	0x0831
#define WATERRECOVERY_FAC_PUMP_TLM_DATA_OUT_MID	0x0832
#define WATERRECOVERY_FAC_PUMP_CMD_MID	0x180E
#define WATERRECOVERY_FAC_RELAY_TLM_DATA_OUT_MID	0x0833
#define WATERRECOVERY_FAC_RELAY_CMD_MID	0x180F
#define WATERRECOVERY_FAC_SOLENOIDVALVE_TLM_DATA_OUT_MID	0x0834
#define WATERRECOVERY_FAC_SOLENOIDVALVE_CMD_MID	0x1812
#define WATERRECOVERY_FAC_THERMOCOUPLE_TLM_DATA_OUT_MID	0x0835
#define ASO_FAULTMANAGEMENT_ACAWS_DE_OUT_IMPACT_REQ_MID	0x0802
#define ASO_FAULTMANAGEMENT_ACAWS_DE_OUT_DIAG_MID	0x0807
#define ASO_FAULTMANAGEMENT_ACAWS_FD_TEST_RESULTS_MID	0x0809
#define ASO_FAULTMANAGEMENT_ACAWS_SE_OUT_IMPACT_MID	0x0811


#endif