/***********************************************************************************
**	File Name:	AMPS_PDU_PDU_1_types.h
**	Generated on: Tue Jul 14 16:44:09 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface AMPS.PDU.PDU_1
***********************************************************************************/

#ifndef _AMPS_PDU_PDU_1_TYPES_H_
#define _AMPS_PDU_PDU_1_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	PDU_UNITID; // unique ID of the component
	uint8	PDU_INPUTBUSSTATUS; // InputBusSTATUS: 4 bit pattern shows state of output register that drives relays
	uint8	PDU_INPUTRELAYFEEDBACK; // InputRelayFeedback: 4 bit pattern shows the actual relay state read from feedback contacts.
	uint8	PDU_INVALIDCOMMANDERROR; // 0= OK, 1 = Error (indicates that more than one input has been selected)
	uint8	PDU_INPUTFEEDBACKSTATEMISMATCHERROR; // 0=OK, 1 = Error (Indicates the On/Off patterns of status register and feedback contact do not agree (possible relay or relay driver fault)
	uint8	PDU_POSNEGMISMATCHERROR; // 0=OK, 1 = Error (Indicates that Positive and Negative relays are not in same state.
	uint8	PDU_RPCCHANNELSTATUS; // 0= OFF 1= ON 8 Bit pattern Indicates state of RPC device for each channel, Note that the channel order is 87654321)
	uint8	PDU_RPCTRIPSTATUS; // 0= OK 1= TRIPPED 8 Bit pattern Indicates that one or more channels have tripped. Note that the channel order is 87654321)
	double	PDU_CURRENTBUS1; // CurrentBus1: Main Bus 1 input currents
	double	PDU_CURRENTBUS2; // CurrentBus2: Main Bus 2 input currents
	double	PDU_CURRENTINTERNALBUS; // CurrentInternalBus: Current inside the PDU upstream of RPCs
	double	PDU_VOLTAGEBUS1; // VoltageBus1: Main bus voltage at input 1
	double	PDU_VOLTAGEBUS2; // VoltageBus2: Main bus voltage at input 2
	double	PDU_VOLTAGEINTERNAL; // VoltageInternal: Voltage inside the PDU upstream of RPCs
	double	PDU_VOLTAGEHKPG; // VoltageHKPG: Voltage of the power supply for the internal controller
	double	PDU_CURRENTRPC1; // CurrentRPC1: Indicates current of each RPC channel
	double	PDU_CURRENTRPC2; // CurrentRPC2: Indicates current of each RPC channel
	double	PDU_CURRENTRPC3; // CurrentRPC3: Indicates current of each RPC channel
	double	PDU_CURRENTRPC4; // CurrentRPC4: Indicates current of each RPC channel
	double	PDU_CURRENTRPC5; // CurrentRPC5: Indicates current of each RPC channel
	double	PDU_CURRENTRPC6; // CurrentRPC6: Indicates current of each RPC channel
	double	PDU_CURRENTRPC7; // CurrentRPC7: Indicates current of each RPC channel
	double	PDU_CURRENTRPC8; // CurrentRPC8: Indicates current of each RPC channel
} AMPS_PDU_PDU_1_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} AMPS_PDU_PDU_1_NoArgCmd_t;

/*
**	User defined command SET_PDU_INPUT_BUS_SELECT
**	select input bus to PDU
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	INPUT_BUS_1;
	uint8	INPUT_BUS_2;
	uint8	INPUT_BUS_1;
	uint8	INPUT_BUS_2;
} AMPS_PDU_PDU_1_SET_PDU_INPUT_BUS_SELECT_t;

/*
**	User defined command SET_PDU_RPC_CHANNEL_STATE
**	select RPC state, which loads
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	RPCONOFFSTATE;
	uint8	RPCCHANNELMASK;
	uint8	RPCONOFFSTATE;
	uint8	RPCCHANNELMASK;
} AMPS_PDU_PDU_1_SET_PDU_RPC_CHANNEL_STATE_t;

/*
**	User defined command SET_PDU_RPC_CHANNEL_OPEN
**	"silent command" to fail open RPC
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
	uint8	FORCERPCOFF;
	uint8	RPCCHANNELMASK;
	uint8	FORCERPCOFF;
	uint8	RPCCHANNELMASK;
} AMPS_PDU_PDU_1_SET_PDU_RPC_CHANNEL_OPEN_t;

#endif