/***********************************************************************************
**	File Name:	ASO_ACAWS_ACAWS_FD_types.h
**	Generated on: Fri Jul 31 13:00:43 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ASO.ACAWS.ACAWS_FD
***********************************************************************************/

#ifndef _ASO_ACAWS_ACAWS_FD_TYPES_H_
#define _ASO_ACAWS_ACAWS_FD_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TEST_RESULTS
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	char	System_TestResult[40];
} ASO_ACAWS_ACAWS_FD_TEST_RESULTS_t;

/*
**	Struct definition for message HK_TLM 
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	char	acaws_fd_hk[64]; /*  need clarification  */
} ASO_ACAWS_ACAWS_FD_HK_TLM__t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_FD_NoArgCmd_t;

/*
**	User defined command acaws_fd_cmd
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_FD_acaws_fd_cmd_t;

/*
**	User defined command acaws_fd_wakeup
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_FD_acaws_fd_wakeup_t;

/*
**	User defined command acaws_fd_snd_hk
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_FD_acaws_fd_snd_hk_t;

#endif