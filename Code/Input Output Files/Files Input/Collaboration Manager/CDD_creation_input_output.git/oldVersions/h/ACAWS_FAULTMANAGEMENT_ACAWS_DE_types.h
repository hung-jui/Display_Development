/***********************************************************************************
**	File Name:	ACAWS_FAULTMANAGEMENT_ACAWS_DE_types.h
**	Generated on: Wed Jul 22 12:58:49 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ACAWS.FAULTMANAGEMENT.ACAWS_DE
***********************************************************************************/

#ifndef _ACAWS_FAULTMANAGEMENT_ACAWS_DE_TYPES_H_
#define _ACAWS_FAULTMANAGEMENT_ACAWS_DE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message OUT_IMPACT_REQ
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	mode_aware;
	uint16	unique_instance_id;
	uint16	failure_list_cnt;
	char	failure_list[240];
} ACAWS_FAULTMANAGEMENT_ACAWS_DE_OUT_IMPACT_REQ_t;

/*
**	Struct definition for message HK_TLM 
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	char	acaws_de_hk[64]; // need clarification
} ACAWS_FAULTMANAGEMENT_ACAWS_DE_HK_TLM _t;

/*
**	Struct definition for message OUT_DIAG
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint8	source_id;
	uint16	sesssion_id;
	uint16	unique_instance_id;
	uint16	aspect_count;
	char	System_HealthStatus[40];
} ACAWS_FAULTMANAGEMENT_ACAWS_DE_OUT_DIAG_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_DE_NoArgCmd_t;

/*
**	User defined command acaws_de_cmd
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_DE_acaws_de_cmd_t;

/*
**	User defined command acaws_de_snd_hk
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} ACAWS_FAULTMANAGEMENT_ACAWS_DE_acaws_de_snd_hk_t;

#endif