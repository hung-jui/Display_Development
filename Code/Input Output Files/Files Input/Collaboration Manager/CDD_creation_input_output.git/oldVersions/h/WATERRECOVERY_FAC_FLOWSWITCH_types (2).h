/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_FLOWSWITCH_types.h
**	Generated on: Wed Jul 22 12:58:49 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.FLOWSWITCH
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_FLOWSWITCH_TYPES_H_
#define _WATERRECOVERY_FAC_FLOWSWITCH_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	FS-CDS-51; // FAC FLOWSWITCH FEED SWITCH
	uint32	FS-CDS-51_VALIDITY; // FAC FLOWSWITCH FEED SWITCH VALIDITY
} WATERRECOVERY_FAC_FLOWSWITCH_TLM_DATA_OUT_t;

#endif