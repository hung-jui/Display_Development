/***********************************************************************************
**	File Name:	PPA_ECLSS_AIR_PPA_types.h
**	Generated on: Wed Jul 22 12:58:48 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface PPA.ECLSS.AIR.PPA
***********************************************************************************/

#ifndef _PPA_ECLSS_AIR_PPA_TYPES_H_
#define _PPA_ECLSS_AIR_PPA_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	mGCA08_Hydrogen;
	float	mGCA09_Oxygen;
	float	mGCA10_Nitrogen;
	float	mGCA11_Carbon_Monoxide;
	float	mGCA12_Methane;
	float	mGCC13_CO2;
	float	mGCC14_Methane;
	float	mGCC15_Ethylene;
	float	mGCC16_Acetylene;
	uint8	gPPASVC132;
	uint8	gPPASVC133;
	uint8	gPPASVC134;
	uint8	SABSV_220;
	uint8	SABSV_221;
	float	gPPA_CH4_CO2_cmd;
	float	gPPA_H2_main_cmd;
	float	gPPA_H2_viewport_cmd;
	float	gPPA_FC104;
	float	gPPA_FC105;
	float	gPPA_FC106;
	float	gPPA_FT104;
	float	gPPA_FT105;
	float	gPPA_FT106;
	float	gPPA_reactor_P_cmd;
	float	gPPA_PT110;
	float	gPPA_P115;
	float	gPPA_P116;
	uint8	gPPA_CO2_regen_cmd;
	uint8	gPPAuWstart;
	float	gPPA_CO2_plasma_uW_cmd;
	float	gPPA_MeP_uW_cmd;
	float	gPPA_Input_Power;
	float	gPPA_Refl_Power;
	uint32	gPPAstub1posn;
	uint32	gPPAstub2posn;
	uint32	gPPAstub3posn;
	uint8	gPPAuWabort;
	float	gPPA_T120;
	float	gPPA_T121;
	float	gPPA_T122;
	float	gPPA_T123;
	float	gPPA_T124;
	float	gPPA_T125;
	float	gPPA_T126;
	uint8	gPPA_LEL_Alarm_State;
	uint8	gPPA_Over_Pressure_Temp_State;
	uint8	CRA_SVC007;
	uint8	CRA_SVC102;
	uint8	CRA_SVO203;
	float	CRA_P002;
	float	CRA_P106;
	float	CRA_P206;
	float	CRA_MFC005_command;
	float	CRA_MFC005_flow;
	float	CRA_MFC103_command;
	float	CRA_MFC103_flow;
	float	CRA_Molar_Ratio;
	float	CRA_P006_1;
	float	CRA_P006_2;
	uint8	CRA_H402_1;
	uint8	CRA_H402_2;
	uint8	CRA_FAN503;
	float	CRA_T403_1;
	float	CRA_T403_2;
	float	CRA_T404_1;
	float	CRA_T404_2;
	float	CRA_P601;
	float	CRA_DP405;
	uint8	CRA_FAN502;
	float	CRA_T407_1;
	float	CRA_T407_2;
	float	CRA_DP409;
	uint8	CRA_PUMP301;
	uint8	CRA_SVO608;
	uint8	CRA_SVO604;
	uint8	CRA_SVO610;
	float	CRA_CG411;
	float	CRA_CG412;
} PPA_ECLSS_AIR_PPA_TLM_t;

#endif