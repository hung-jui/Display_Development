/***********************************************************************************
**	File Name:	ASO_FAULTMANAGEMENT_ACAWS_FD_types.h
**	Generated on: Thu Jul 16 10:21:19 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ASO.FAULTMANAGEMENT.ACAWS_FD
***********************************************************************************/

#ifndef _ASO_FAULTMANAGEMENT_ACAWS_FD_TYPES_H_
#define _ASO_FAULTMANAGEMENT_ACAWS_FD_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TEST_RESULTS
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	char	System_TestResult[40];
} ASO_FAULTMANAGEMENT_ACAWS_FD_TEST_RESULTS_t;

#endif