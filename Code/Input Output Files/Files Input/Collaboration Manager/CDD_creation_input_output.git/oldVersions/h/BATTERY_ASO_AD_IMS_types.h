/***********************************************************************************
**	File Name:	BATTERY_ASO_AD_IMS_types.h
**	Generated on: Wed Jul 22 12:58:48 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface BATTERY.ASO.AD_IMS
***********************************************************************************/

#ifndef _BATTERY_ASO_AD_IMS_TYPES_H_
#define _BATTERY_ASO_AD_IMS_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message IMS_STD_ORION_BATT_TLM
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	TBD;
} BATTERY_ASO_AD_IMS_IMS_STD_ORION_BATT_TLM_t;

/*
**	Struct definition for message IMS_HK_TLM
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	uint32	TLM;
} BATTERY_ASO_AD_IMS_IMS_HK_TLM_t;

/*
**	Struct definition for message IMS_OUT_DATA
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; // CFS Header
	float	Timestamp;
	float	Anomaly_score;
	float	Contribution;
} BATTERY_ASO_AD_IMS_IMS_OUT_DATA_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} BATTERY_ASO_AD_IMS_NoArgCmd_t;

/*
**	User defined command IMS_CMD
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} BATTERY_ASO_AD_IMS_IMS_CMD_t;

/*
**	User defined command IMS_SEND_HK
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; // CFS Header
} BATTERY_ASO_AD_IMS_IMS_SEND_HK_t;

#endif