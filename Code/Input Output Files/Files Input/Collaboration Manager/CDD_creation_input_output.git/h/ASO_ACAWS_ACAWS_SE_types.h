/***********************************************************************************
**	File Name:	ASO_ACAWS_ACAWS_SE_types.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface ASO.ACAWS.ACAWS_SE
***********************************************************************************/

#ifndef _ASO_ACAWS_ACAWS_SE_TYPES_H_
#define _ASO_ACAWS_ACAWS_SE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message HK_TLM 
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	char	acaws_se_hk[64]; /*  need clarification  */
} ASO_ACAWS_ACAWS_SE_HK_TLM__t;

/*
**	Struct definition for message OUT_DATA
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	uint16	unique_instance_id;
	char	acaws_se_effects[960]; /*  acaws_se_effects is of type ACAWS_SE_Effects_t, which in turn is an array of type ACAWS_SE_ImpactType_t (size = 4 bytes). The max size of the array is 24 making this field 24*4 = 96 bytes. Then we multiple by DIAG_MAX_ELEM_SIZE that currently is 10.  */
} ASO_ACAWS_ACAWS_SE_OUT_DATA_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_SE_NoArgCmd_t;

/*
**	User defined command acaws_se_cmd
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_SE_acaws_se_cmd_t;

/*
**	User defined command acaws_se_snd_hk
**	
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} ASO_ACAWS_ACAWS_SE_acaws_se_snd_hk_t;

#endif