/***********************************************************************************
**	File Name:	WATERRECOVERY_FAC_THERMOCOUPLE_types.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.FAC.THERMOCOUPLE
***********************************************************************************/

#ifndef _WATERRECOVERY_FAC_THERMOCOUPLE_TYPES_H_
#define _WATERRECOVERY_FAC_THERMOCOUPLE_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	float	TT_CDS_04; /*  FAC THERMOCOUPLE COOLANT  */
	uint32	TT_CDS_04_VALIDITY; /*  FAC THERMOCOUPLE COOLANT VALIDITY  */
	float	FAC_7409TB_1_CJ; /*  FAC THERMOCOUPLE FAC 7409TB 1 CJ  */
	uint32	FAC_7409TB_1_CJ_VALIDITY; /*  FAC THERMOCOUPLE FAC 7409TB 1 CJ VALIDITY  */
} WATERRECOVERY_FAC_THERMOCOUPLE_TLM_DATA_OUT_t;

#endif