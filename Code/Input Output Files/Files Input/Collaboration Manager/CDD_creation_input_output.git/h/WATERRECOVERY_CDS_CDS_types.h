/***********************************************************************************
**	File Name:	WATERRECOVERY_CDS_CDS_types.h
**	Generated on: Thu Aug 06 13:02:54 CDT 2015
**	Purpose: The purpose of this file is to define all structs for all command and telemetry messages for software interface WATERRECOVERY.CDS.CDS
***********************************************************************************/

#ifndef _WATERRECOVERY_CDS_CDS_TYPES_H_
#define _WATERRECOVERY_CDS_CDS_TYPES_H_


#include "cfe.h"



/*
**	Struct definition for message TLM_DATA_OUT
*/
typedef struct {
	uint8	ucTlmHeader[CFE_SB_TLM_HDR_SIZE]; /*  CFS Header  */
	float	CD_VOLTS; /*  CDS CDS CD VOLTS  */
	uint32	CD_VOLTS_VALIDITY; /*  CDS CDS CD VOLTS VALIDITY  */
	float	CD_SPEED; /*  CDS CDS DISTILLER  */
	uint32	CD_SPEED_VALIDITY; /*  CDS CDS DISTILLER VALIDITY  */
	float	CD1_5; /*  CDS CDS CD1 5  */
	uint32	CD1_5_VALIDITY; /*  CDS CDS CD1 5 VALIDITY  */
	uint32	UPS_CDS_1; /*  CDS CDS UMC  */
	uint32	UPS_CDS_1_VALIDITY; /*  CDS CDS UMC VALIDITY  */
	uint32	UPS_CDS_2; /*  CDS CDS CDS  */
	uint32	UPS_CDS_2_VALIDITY; /*  CDS CDS CDS VALIDITY  */
} WATERRECOVERY_CDS_CDS_TLM_DATA_OUT_t;

/*
**	No arg command
**	Intended to be used for SEND_HK & WAKEUP commands
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
} WATERRECOVERY_CDS_CDS_NoArgCmd_t;

/*
**	User defined command CDS
**	CDS CDS DISTILLER
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	CONTROL_ARG;
} WATERRECOVERY_CDS_CDS_CDS_t;

/*
**	User defined command THP
**	CDS CDS THP
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	SWITCH_ARG;
} WATERRECOVERY_CDS_CDS_THP_t;

/*
**	User defined command PWE
**	CDS CDS PWE
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	uint8	POWER_ARG;
} WATERRECOVERY_CDS_CDS_PWE_t;

/*
**	User defined command PWV
**	CDS CDS PWV
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	float	VOLTAGE_CONTROL;
} WATERRECOVERY_CDS_CDS_PWV_t;

/*
**	User defined command PWI
**	CDS CDS PWI
*/
typedef struct {
	uint8	ucCmdHeader[CFE_SB_CMD_HDR_SIZE]; /*  CFS Header  */
	float	CURRENT_LIMIT_CONTROL;
} WATERRECOVERY_CDS_CDS_PWI_t;

#endif