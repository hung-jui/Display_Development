﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="CDS_Custom_Indicators" Type="Folder">
			<Item Name="Right_Left_Boolean.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/Right_Left_Boolean.ctl"/>
			<Item Name="SV1_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV1_Valve.ctl"/>
			<Item Name="SV2_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV2_Valve.ctl"/>
			<Item Name="SV3_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV3_Valve.ctl"/>
			<Item Name="SV4_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV4_Valve.ctl"/>
			<Item Name="SV5_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV5_Valve.ctl"/>
			<Item Name="SV6_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV6_Valve.ctl"/>
			<Item Name="SV7_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV7_Valve.ctl"/>
			<Item Name="SV8_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV8_Valve.ctl"/>
			<Item Name="SV9_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV9_Valve.ctl"/>
			<Item Name="SV10_Valve.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/SV10_Valve.ctl"/>
			<Item Name="Feed_Tank.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/Feed_Tank.ctl"/>
			<Item Name="Horz_Vert_Boolean.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/Horz_Vert_Boolean.ctl"/>
			<Item Name="One_Two.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/One_Two.ctl"/>
			<Item Name="DownTurn_UpTurn_Boolean.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Custom_Indicators/DownTurn_UpTurn_Boolean.ctl"/>
		</Item>
		<Item Name="CDS_Display_Front_End.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Display_Front_End.vi"/>
		<Item Name="CDS_Display_Front_End_Telemetry.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/CDS_Display_Front_End_Telemetry.vi"/>
		<Item Name="Read_Me_CDS_Display_lvproj.pdf" Type="Document" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/Read_Me_CDS_Display_lvproj.pdf"/>
		<Item Name="Read_Me_CDS_Display_lvproj.docx" Type="Document" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/Read_Me_CDS_Display_lvproj.docx"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Get Queued Value.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/AMPS Displays/PDU Reusable Elements/Get Queued Value.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CDS_Display" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{AFB46B46-BDE1-4582-BCC3-182AD5702CE2}</Property>
				<Property Name="App_INI_GUID" Type="Str">{50929AB3-0F18-4C74-8D5C-D28B3E9264F9}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8203DEF5-9164-4739-B0EC-371053C1E21C}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CDS_Display</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/CDS_Display</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{8D16C481-FCAC-4CBA-A72B-005E793657D0}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CDS_Display.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/CDS_Display/CDS_Display.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/CDS_Display/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{C232D364-5BC1-4395-8713-48F1AC440A7B}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CDS_Display_Front_End.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Hewlett-Packard Company</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CDS_Display</Property>
				<Property Name="TgtF_internalName" Type="Str">CDS_Display</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 Hewlett-Packard Company</Property>
				<Property Name="TgtF_productName" Type="Str">CDS_Display</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{4FDCF9A4-F166-4B81-AE4F-07159184375B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CDS_Display.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
