﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="varPersistentID:{F58E8B87-3C96-4DDD-A449-85FC2C3C6FAB}" Type="Ref">/My Computer/Audio Library.lvlib/Table_Caller</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Audio_Custom_Controls" Type="Folder">
			<Item Name="Active_Button.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Active_Button.ctl"/>
			<Item Name="Audio_Config_Selection.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Audio_Config_Selection.ctl"/>
			<Item Name="Audio_Mode.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Audio_Mode.ctl"/>
			<Item Name="Settings_Button.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Settings_Button.ctl"/>
			<Item Name="Testing_Button.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Testing_Button.ctl"/>
			<Item Name="Volume_Control.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Volume_Control.ctl"/>
			<Item Name="Get Value STRING.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Get Value STRING.vi"/>
		</Item>
		<Item Name="Audio Control Unit Navigation" Type="Folder">
			<Item Name="Loop_1_Mode_CDR.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio Control Unit Navigation/Loop_1_Mode_CDR.vi"/>
			<Item Name="Home_Screen.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio Control Unit Navigation/Home_Screen.vi"/>
		</Item>
		<Item Name="Audio_Display_Front.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Front.vi"/>
		<Item Name="Audio_Display_Front_Display_Current.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/CDS_Display/Audio_Display_Front_Display_Current.vi"/>
		<Item Name="Audio_Display_Front_Mini_Map.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Front_Mini_Map.vi"/>
		<Item Name="Audio_Test_Display.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Test_Display.vi"/>
		<Item Name="Read_Me_Audio_Display_lvproj.pdf" Type="Document" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Read_Me_Audio_Display_lvproj.pdf"/>
		<Item Name="Read_Me_Audio_Display_lvproj.docx" Type="Document" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Read_Me_Audio_Display_lvproj.docx"/>
		<Item Name="How_To_Make_An_Audio_Custom_Control.pdf" Type="Document" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/How_To_Make_An_Audio_Custom_Control.pdf"/>
		<Item Name="How_To_Make_An_Audio_Custom_Control.docx" Type="Document" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/How_To_Make_An_Audio_Custom_Control.docx"/>
		<Item Name="Audio Library.lvlib" Type="Library" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio Library.lvlib"/>
		<Item Name="Primary_Color.ctl" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/Audio_Display/Audio_Display_Controls/Primary_Color.ctl"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Get Queued Value.vi" Type="VI" URL="/&lt;userlib&gt;/2015 Spring - Kirsi Kuutti/AMPS Displays/PDU Reusable Elements/Get Queued Value.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Audio_Display_Front_Executable" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{8C95677D-5201-4859-B61C-8D89A3644DDD}</Property>
				<Property Name="App_INI_GUID" Type="Str">{B11C32C4-E227-475A-A5B1-376E8B147EBC}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{06F60898-1A66-4477-84D2-4B50670892D4}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Audio_Display_Front_Executable</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Executable</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C02605AE-4567-4C12-945C-7A43A5487551}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Audio_Display_Front_Executable.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Executable/Audio_Display_Front_Executable.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Executable/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{F8D8210C-73A6-443E-A8B3-FB807C63A554}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Audio_Display_Front.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Hewlett-Packard Company</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Audio_Display_Front_Executable</Property>
				<Property Name="TgtF_internalName" Type="Str">Audio_Display_Front_Executable</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 Hewlett-Packard Company</Property>
				<Property Name="TgtF_productName" Type="Str">Audio_Display_Front_Executable</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{97A07177-AA2C-4379-B000-CECDE0723783}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Audio_Display_Front_Executable.exe</Property>
			</Item>
			<Item Name="Audio_Display_Front_Display_Current_Executable" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{340D649C-07FC-4091-8326-3456613453F1}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A8414160-910E-4714-9214-C1C1F83C69F9}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8C4C661C-2175-4BDF-95E4-BD4978D0CE64}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Audio_Display_Front_Display_Current_Executable</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Display_Current_Executable</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{F615946D-A407-457C-B59E-42F9A3A5935B}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Audio_Display_Front_Display_Current_Executable.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Display_Current_Executable/Audio_Display_Front_Display_Current_Executable.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Display_Current_Executable/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{F8D8210C-73A6-443E-A8B3-FB807C63A554}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Audio_Display_Front_Display_Current.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Hewlett-Packard Company</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Audio_Display_Front_Display_Current_Executable</Property>
				<Property Name="TgtF_internalName" Type="Str">Audio_Display_Front_Display_Current_Executable</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 Hewlett-Packard Company</Property>
				<Property Name="TgtF_productName" Type="Str">Audio_Display_Front_Display_Current_Executable</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A5063B23-6B48-4B61-AA9E-2F7F6AF093C9}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Audio_Display_Front_Display_Current_Executable.exe</Property>
			</Item>
			<Item Name="Audio_Display_Front_Mini_Map_Executable" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{C1E30EE9-3DB7-4C32-82B2-BB322434551B}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A4F69F14-A470-4CC4-BE40-752EE3A88DCB}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{59260BFD-5D10-491E-8CEA-DFCF8A8C29DD}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Audio_Display_Front_Mini_Map_Executable</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Mini_Map_Executable</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{78FB998E-BE56-4A72-9589-131E393F5EAD}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Audio_Display_Front_Mini_Map_Executable.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Mini_Map_Executable/Audio_Display_Front_Mini_Map_Executable.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Display_Front_Mini_Map_Executable/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{F8D8210C-73A6-443E-A8B3-FB807C63A554}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Audio_Display_Front_Mini_Map.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Hewlett-Packard Company</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Audio_Display_Front_Mini_Map_Executable</Property>
				<Property Name="TgtF_internalName" Type="Str">Audio_Display_Front_Mini_Map_Executable</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 Hewlett-Packard Company</Property>
				<Property Name="TgtF_productName" Type="Str">Audio_Display_Front_Mini_Map_Executable</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A97F85CB-5FB3-470E-B8E2-5AD84143F422}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Audio_Display_Front_Mini_Map_Executable.exe</Property>
			</Item>
			<Item Name="Audio_Test_Display_Executable" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{B47ECADC-C566-4960-9DDD-47C68DA4038D}</Property>
				<Property Name="App_INI_GUID" Type="Str">{838D7252-760B-41F1-9747-CD4FACF72E89}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{7436FD0B-7950-4FCD-ACF8-B4DB2DEBD10A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Audio_Test_Display_Executable</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Test_Display_Executable</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{2462AC0D-4BA7-4AA7-AA12-5606421E3818}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Audio_Test_Display_Executable.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Test_Display_Executable/Audio_Test_Display_Executable.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Audio_Test_Display_Executable/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{F8D8210C-73A6-443E-A8B3-FB807C63A554}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Audio_Test_Display.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Hewlett-Packard Company</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Audio_Test_Display_Executable</Property>
				<Property Name="TgtF_internalName" Type="Str">Audio_Test_Display_Executable</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 Hewlett-Packard Company</Property>
				<Property Name="TgtF_productName" Type="Str">Audio_Test_Display_Executable</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9053A9D2-BE34-458A-AE12-71572794C58C}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Audio_Test_Display_Executable.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
